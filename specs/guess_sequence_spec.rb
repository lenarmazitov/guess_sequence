describe "GuessSequence" do
  before(:all) do
    require './guess_sequence.rb'
  end

  describe "GuessSequenceModule.slice_when" do
    it "should slice array to chunks by condition" do
      actual = [1, 1, 2, 2, 1, 2, 2].slice_when do |cur, nxt|
        cur != nxt
      end
      expected = [[1, 1], [2, 2], [1], [2, 2]]
      expect(actual).to eq expected
    end
  end

  describe "GuessSequence.value" do
    it "should return valid sequence" do
      seq = GuessSequence.new(5)
      expect(seq.to_a).to eq [1, 11, 21, 1211, 111221, 312211]
    end
  end
end