module SomeArrayExtensions
  def slice_when
    result = []
    piece = []

    self.each_index do |i|
      piece.push(self[i])
      if self[i + 1].nil? || yield(self[i], self[i + 1])
        result.push(piece)
        piece = []
      end
    end
    result
  end
end

class Array
  include SomeArrayExtensions
end