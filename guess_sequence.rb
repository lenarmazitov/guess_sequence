require './modules/some_array_extensions'

class GuessSequence

  def initialize(sequence_length)
    @sequence_length = sequence_length
    @initial_value = 1
  end

  def to_a
    return compute
  end

  private

    def compute
      result_arr = [@initial_value]

      next_value = @initial_value
      (0...@sequence_length).each do
        next_value = make_next(next_value)
        result_arr.push(next_value)
      end

      result_arr
    end

    def make_next(number)
      # преобразуем наше число в последовательность цифр
      digits_array = number.to_s.split("").map { |elem| elem.to_i }
      slices = digits_array.slice_when do |cur, nxt|
        cur != nxt
      end

      intermediate_result = []
      slices.each do |elem|
        intermediate_result.push(elem.length)
        intermediate_result.push(elem.first)
      end

      intermediate_result.join("").to_i
    end
end